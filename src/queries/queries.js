import { gql } from 'apollo-boost';

const getOriginsQuery = gql`
    {
        origins {
            name
            id
        }
    }
`;

const getChemosQuery = gql`
    {
        chemos {
            name
            id
            geno {
                genome
            }
            pheno {
                climate
                behavior
            }
            origin {
                id
                name
                chemos {
                    id
                    name
                }
            }
        }
    }
`;

const addChemoMutation = gql`
    mutation AddChemo($name: String!, $genome: String!, $climate: String!, $behavior: String!, $originId: ID!){
        addChemo(name: $name, genome: $genome, climate: $climate, behavior: $behavior, originId: $originId){
            name
            id
        }
    }
`;

const addChemoNewOriginMutation = gql`
    mutation AddChemoNewOrigin($name: String!, $genome: String!, $climate: String!, $behavior: String!, $originName: String!){
        addChemoNewOrigin(name: $name, genome: $genome, climate: $climate, behavior: $behavior, originName: $originName){
            name
            id
        }
    }
`;

const getChemoQuery = gql`
    query GetChemo($id: ID){
        chemo(id: $id) {
            id
            name
            geno {
                id
                genome
                maGenoId
                paGenoId
            }
            pheno {
                climate
                behavior
            }
            origin {
                id
                name
                chemos {
                    id
                    name
                }
            }
        }
    }
`;
// 
export { getOriginsQuery, getChemosQuery, addChemoMutation, addChemoNewOriginMutation, getChemoQuery };
