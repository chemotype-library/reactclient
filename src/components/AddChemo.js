import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import { getOriginsQuery, addChemoMutation, addChemoNewOriginMutation, getChemosQuery } from '../queries/queries';

class AddChemo extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            genome: '',
            climate: '',
            behavior: '',
            originId: '',
            originName: '',
            showOriginName: false
        };
    }
    displayOrigins(){
        var data = this.props.getOriginsQuery;
        if(data.loading){
            return( <option disabled>Loading origins</option> );
        } else {
            return data.origins.map(origin => {
                return( <option key={ origin.id } value={origin.id}>{ origin.name }</option> );
            });
        }
    }
    handleOriginChange = (e) => {
        if(e.target.value === "newOrigin") {
            this.setState({ showOriginName: true });
        } else {
            this.setState({ showOriginName: false });
        }
        this.setState({ originId: e.target.value })
    }
    submitForm = (e) => {
        if(this.state.originId === "newOrigin") {
            this.props.addChemoNewOriginMutation({
                variables: {
                    name: this.state.name,
                    genome: this.state.genome,
                    climate: this.state.climate,
                    behavior: this.state.behavior,
                    originName: this.state.originName
                },
                refetchQueries: [{ query: getChemosQuery }]
            })
        } else {
            this.props.addChemoMutation({
                variables: {
                    name: this.state.name,
                    genome: this.state.genome,
                    climate: this.state.climate,
                    behavior: this.state.behavior,
                    originId: this.state.originId
                },
                refetchQueries: [{ query: getChemosQuery }]
            });
        }
    }
    render(){
        return(
            <form id="add-chemo" onSubmit={ this.submitForm.bind(this) } onLoad={ this.displayChemicals }>
                <div className="field">
                    <label>Name:</label>
                    <input type="text" onChange={ (e) => this.setState({ name: e.target.value }) } />
                </div>
                <div className="field">
                    <label>Genome:</label>
                    <input type="text" onChange={ (e) => this.setState({ genome: e.target.value }) } />
                </div>
                <div className="field">
                    <label>Climate:</label>
                    <input type="text" onChange={ (e) => this.setState({ climate: e.target.value }) } />
                </div>
                <div className="field">
                    <label>Behavior:</label>
                    <input type="text" onChange={ (e) => this.setState({ behavior: e.target.value }) } />
                </div>
                <div className="field">
                    <label>Origin:</label>
                    <select onChange={ this.handleOriginChange } >
                        <option>Select origin</option>
                        <option value="newOrigin">New Origin...</option>
                        { this.displayOrigins() }
                    </select>
                </div>
                <div className="field" >
                    <label hidden={ !this.state.showOriginName ?  true : false } >Origin Name:</label>
                    <input type="text" hidden={ !this.state.showOriginName ?  true : false } onChange={ (e) => this.setState({ originName: e.target.value }) } />
                </div>
                <button>+</button>
            </form>
        );
    }
}

export default compose(
    graphql(getOriginsQuery, { name: "getOriginsQuery" }),
    graphql(addChemoMutation, { name: "addChemoMutation" }),
    graphql(addChemoNewOriginMutation, { name: "addChemoNewOriginMutation" })
)(AddChemo);
