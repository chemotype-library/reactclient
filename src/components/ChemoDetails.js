import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { getChemoQuery } from '../queries/queries';

class ChemoDetails extends Component {
    displayChemoDetails(){
        const { chemo } = this.props.data;
        if(chemo){
            return(
                <div>
                    <h2>Name: { chemo.name }</h2>
                    <h3>Origin: { chemo.origin.name }</h3>
                    <h3>Genome: { chemo.geno.genome }</h3>
                    <h3>Climate: { chemo.pheno.climate }</h3>
                    <h3>Behavior: { chemo.pheno.behavior }</h3>
                    <p>All chemos from this origin:</p>
                    <ul className="other-chemos">
                        { chemo.origin.chemos.map(item => {
                            return <li key={item.id}>{ item.name }</li>
                        })}
                    </ul>
                </div>
            );
        } else {
            return( <div>No Cultivar selected...</div> );
        }
    }
    render(){
        return(
            <div id="chemo-details">
                { this.displayChemoDetails() }
            </div>
        );
    }
}

export default graphql(getChemoQuery, {
    options: (props) => {
        return {
            variables: {
                id: props.chemoId
            }
        }
    }
})(ChemoDetails);
