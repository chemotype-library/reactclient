import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { getChemosQuery } from '../queries/queries';

// components
import ChemoDetails from './ChemoDetails';

class ChemoList extends Component {
    constructor(props){
        super(props);
        this.state = {
            selected: null
        }
    }
    displayChemos(){
        var data = this.props.data;
        if(data.loading){
            return( <div>Loading Cultivars...</div> );
        } else {
            return data.chemos.map(chemo => {
                return(
                    <li key={ chemo.id } onClick={ (e) => this.setState({ selected: chemo.id }) }>{ chemo.name }</li>
                );
            })
        }
    }
    render(){
        return(
            <div>
                <ul id="chemo-list">
                    { this.displayChemos() }
                </ul>
                <ChemoDetails chemoId={ this.state.selected } />
            </div>
        );
    }
}

export default graphql(getChemosQuery)(ChemoList);
