import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

// components
import ChemoList from './components/ChemoList';
import AddChemo from './components/AddChemo';

// apollo client setup
const client = new ApolloClient({
    uri: 'http://localhost:4000/graphql'
});

class App extends Component {
  render() {
    return (
        <ApolloProvider client={client}>
            <div id="main">
                <h1>Cultivar Library</h1>
                <ChemoList />
                <AddChemo />
            </div>
        </ApolloProvider>
    );
  }
}

export default App;
